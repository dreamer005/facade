package facade;

public class FacadePattern {
	public static void main(String args[]){  
	      ShopKeeper sk=new ShopKeeper();  
	      sk.iphoneSale();  
	      sk.samsungSale();    
	      sk.blackberrySale();
	    }
}
